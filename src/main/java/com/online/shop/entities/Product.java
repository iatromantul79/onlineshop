package com.online.shop.entities;


import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String category;
    private Double price;
    private String description;
    private Integer quantity;


    @Lob
    @Column(columnDefinition = "BLOB")
    private byte[] image;

//    @ManyToMany
//    @JoinTable(name = "product_shoppingCart",
//            joinColumns = @JoinColumn(name = "id"),
//    inverseJoinColumns =  @JoinColumn(name = "shoppingCartId"))

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<ChosenProduct> chosenProducts;
}

