package com.online.shop.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
public class ChosenProduct {

    @Id
    @GeneratedValue
    private Integer chosenProductId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private ShoppingCart shoppingCart;

    @ManyToOne
    @JoinColumn
    private CustomerOrder customerOrder;

    private Integer chosenQuantity;

    @ManyToOne
    @JoinColumn
    private Product product;
}
