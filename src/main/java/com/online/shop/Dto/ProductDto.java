package com.online.shop.Dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {
    private String name;
    private String cathegory;
    private String price;
    private String description;
    private String image;
    private String id;
    private String quantity;
}

