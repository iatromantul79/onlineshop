package com.online.shop.Dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChosenProductDto {

    private String quantity;
}
