package com.online.shop.Dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShoppingCartItemDto {

    private String name;
    private String image;
    private String price;
    private String quantity;
    private String total;

}
