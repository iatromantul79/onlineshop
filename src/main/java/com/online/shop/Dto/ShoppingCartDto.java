package com.online.shop.Dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class ShoppingCartDto {

   private String subTotal;

   private String Total;

   private List<ShoppingCartItemDto> items;

   public ShoppingCartDto(){
      items= new ArrayList<>();
   }

   public void add(ShoppingCartItemDto shoppingCartItemDto){
      items.add(shoppingCartItemDto);
   }
}
