package com.online.shop.service;


import com.online.shop.entities.ChosenProduct;
import com.online.shop.entities.CustomerOrder;
import com.online.shop.entities.ShoppingCart;
import com.online.shop.entities.User;
import com.online.shop.repository.ChosenProductRepository;
import com.online.shop.repository.CustomerOrderRepository;
import com.online.shop.repository.ShoppingCartRepository;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerOrderService {

    @Autowired
    private UserRepository userRepository;

    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ChosenProductRepository chosenProductRepository;

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    public  void addCustomerOrder(String loggedInuserEmail, String shippingAddress){

        Optional<User> optionalUser = userRepository.findByEmail(loggedInuserEmail);
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInuserEmail);
        User user = optionalUser.get();
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setUser(user);
        customerOrder.setShippingAdress(user.getAddress());
        customerOrderRepository.save(customerOrder);


        for (ChosenProduct chosenProduct : shoppingCart.getChosenProducts()){
            chosenProduct.setShoppingCart(null);
            chosenProduct.setCustomerOrder(customerOrder);
            chosenProductRepository.save(chosenProduct);
        }
    }
}
